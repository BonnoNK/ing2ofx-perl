ing2ofx
=======

Converteer de exports vanuit mijn.ing.nl naar het ofx-formaat waar GnuCash mee om kan gaan.
Dit script komt van http://blog.maashoek.nl/2009/07/gnucash-en-internetbankieren/ (offline: http://web.archive.org/web/20120912002948/http://blog.maashoek.nl/2009/07/gnucash-en-internetbankieren) en http://fam-de-jong.nl/WordPress/?p=142 vandaan.

## Gebruik ##
`pb2ofx.pl reknr_27-06-2009_09-07-2009.csv`

levert een file reknr_27-06-2009_09-07-2009.ofx in de directory ofx.


## Changelist ##
* Eigen rekeningnummer IBAN-proof gemaakt
* De transactie ID is nu (zo goed als) uniek.